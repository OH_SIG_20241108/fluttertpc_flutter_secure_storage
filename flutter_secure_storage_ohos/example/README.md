# flutter_secure_storage_ohos_example

此示例展示如何使用flutter_secure_storage_ohos插件。

## 开始使用

本项目作为Flutter应用程序的起始模板。

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
